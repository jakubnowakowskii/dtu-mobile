package pl.wat.wcy.checksumprojectmain.utils;

import android.content.res.AssetManager;

import org.apache.commons.lang3.SerializationUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Collectors;

public class Md5Calculator {

    private static final String MD5_ALGORITHM_NAME = "MD5";

    public <T extends Serializable> String calculateMd5ForCollection(Collection<T> collection) {
        byte[] bytes = convertCollectionToBytes(collection);
        return calculateMd5(bytes);
    }

    private <T extends Serializable> byte[] convertCollectionToBytes(Collection<T> collection) {
        return collection.stream()
                .map(SerializationUtils::serialize)
                .map(Arrays::toString)
                .collect(Collectors.joining())
                .getBytes();
    }

    public String calculateMd5ForFile(String fileName, AssetManager assets) {
        try {
            byte[] buffer = readFileFromAssets(fileName, assets);
            return calculateMd5(buffer);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private byte[] readFileFromAssets(String fileName, AssetManager assets) throws IOException {
        InputStream inputStream = assets.open(fileName);
        int size = inputStream.available();
        byte[] buffer = new byte[size];
        inputStream.read(buffer);
        inputStream.close();
        return buffer;
    }

    private String calculateMd5(byte[] contentBytes) {
        MessageDigest m;
        try {
            m = MessageDigest.getInstance(MD5_ALGORITHM_NAME);
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("MD5 Algorithm initialization exception.", e);
        }
        m.update(contentBytes, 0, contentBytes.length);
        return (new BigInteger(1, m.digest()).toString(16));
    }
}
