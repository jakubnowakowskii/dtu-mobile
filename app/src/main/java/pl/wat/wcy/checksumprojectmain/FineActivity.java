package pl.wat.wcy.checksumprojectmain;

import android.os.Bundle;
import android.widget.TextView;

import java.util.List;
import java.util.concurrent.ExecutionException;

import androidx.appcompat.app.AppCompatActivity;
import pl.wat.wcy.checksumprojectmain.domain.entity.User;
import pl.wat.wcy.checksumprojectmain.domain.repository.UserRepository;

import static java.util.Objects.nonNull;

public class FineActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fine);

        List<User> users = null;
        try {
            users = new UserRepository().getAll(this).get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        if (nonNull(users) && users.size() > 0) {
            ((TextView) findViewById(R.id.user)).setText(users.get(0).firstName + " " + users.get(0).lastName);
        }
    }

}
