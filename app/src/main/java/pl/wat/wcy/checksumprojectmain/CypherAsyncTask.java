package pl.wat.wcy.checksumprojectmain;

import android.os.AsyncTask;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Scanner;

import javax.net.ssl.HttpsURLConnection;

public class CypherAsyncTask extends AsyncTask<Void, Void, String> {

    private final String endpoint;

    public CypherAsyncTask(String endpoint) {
        this.endpoint = endpoint;
    }

    @Override
    protected void onPreExecute() {

    }

    @Override
    protected String doInBackground(Void... params) {
        String serviceChecksum = null;

        try {
            URL url = new URL(endpoint);
            HttpsURLConnection connection =
                    (HttpsURLConnection) url.openConnection();

            if (connection.getResponseCode() == 200) {
                serviceChecksum = getStringAsResponse(connection);
            }

            connection.disconnect();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return serviceChecksum;
    }

    private String getStringAsResponse(HttpsURLConnection connection) throws IOException {
        String serviceChecksum = null;
        InputStream responseBody = connection.getInputStream();
        //TODO zobaczyc czy zadziala bez useDelimiter
        Scanner s = new Scanner(responseBody);
//                Scanner s = new Scanner(responseBody).useDelimiter("\\A");
        if (s.hasNext()) {
            serviceChecksum = s.next();
        }
        return serviceChecksum;
    }

    @Override
    protected void onPostExecute(String result) {
    }


}