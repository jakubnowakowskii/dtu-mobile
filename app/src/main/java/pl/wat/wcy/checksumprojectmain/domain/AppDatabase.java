package pl.wat.wcy.checksumprojectmain.domain;

import android.content.Context;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;
import pl.wat.wcy.checksumprojectmain.domain.dao.UserDao;
import pl.wat.wcy.checksumprojectmain.domain.entity.User;

@Database(entities = {User.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {

    private static final String DB_NAME = "db";

    private static volatile AppDatabase INSTANCE;

    private static RoomDatabase.Callback sRoomDatabaseCallback =
            new RoomDatabase.Callback() {

                @Override
                public void onOpen(@NonNull SupportSQLiteDatabase db) {
                    super.onOpen(db);
                    new PopulateDbAsync(INSTANCE).execute();
                }
            };

    public static AppDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (AppDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context,
                            AppDatabase.class, DB_NAME)
                            .addCallback(sRoomDatabaseCallback)
                            .build();
                    INSTANCE.query("select 1", null);
                }
            }
        }
        return INSTANCE;
    }

    public abstract UserDao userDao();

    private static class PopulateDbAsync extends AsyncTask<Void, Void, Void> {

        private final UserDao mDao;

        PopulateDbAsync(AppDatabase db) {
            mDao = db.userDao();
        }

        @Override
        protected Void doInBackground(final Void... params) {
            populateDatabase();
            return null;
        }

        private void populateDatabase() {
            mDao.deleteAll();
            User user = new User(1, "Jan", "Kowalski");
            mDao.insertAll(user);
        }
    }
}


