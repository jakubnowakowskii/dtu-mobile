package pl.wat.wcy.checksumprojectmain.utils;

import android.content.Context;
import android.content.res.AssetManager;
import android.util.Log;

import java.util.List;
import java.util.concurrent.ExecutionException;

import pl.wat.wcy.checksumprojectmain.CypherAsyncTask;
import pl.wat.wcy.checksumprojectmain.domain.entity.User;
import pl.wat.wcy.checksumprojectmain.domain.repository.UserRepository;

import static java.util.Objects.isNull;

public class DataIntegrityTester {

    private static final String TAG = DataIntegrityTester.class.getSimpleName();
    private static final String FILE_NAME = "file.txt";

    private static final String BASE_ENDPOINT = "https://files-integration.herokuapp.com/";
    private static final String FILE_HASH_ENDPOINT = BASE_ENDPOINT + "file/hash";
    private static final String DB_HASH_ENDPOINT = BASE_ENDPOINT + "database/hash";

    private final Md5Calculator md5Calculator = new Md5Calculator();

    private final UserRepository userRepository = new UserRepository();

    public boolean testForIntegrity(Context context) {
        return testForFileIntegrity(context.getAssets()) && testForDbIntegrity(context);
    }

    private boolean testForDbIntegrity(Context context) {
        List<User> users;
        try {
            users = userRepository.getAll(context).get();
        } catch (InterruptedException | ExecutionException e) {
            throw new RuntimeException("Cannot access to database.", e);
        }
        String dbChecksum = md5Calculator.calculateMd5ForCollection(users);
        Log.d(TAG, "Calculated db hash: " + dbChecksum);
        return testForDataIntegrity(dbChecksum, DB_HASH_ENDPOINT);
    }

    private boolean testForFileIntegrity(AssetManager assets) {
        String fileChecksum = md5Calculator.calculateMd5ForFile(FILE_NAME, assets);
        Log.d(TAG, "Calculated file (" + FILE_NAME + ") hash: " + fileChecksum);
        return testForDataIntegrity(fileChecksum, FILE_HASH_ENDPOINT);
    }

    private boolean testForDataIntegrity(String checksum, String serviceEndpoint) {
        if (isNull(checksum)) {
            Log.e(TAG, "Hash is not calculated");
            return false;
        }

        String fileChecksumFromService = loadChecksumFromService(serviceEndpoint);
        Log.d(TAG, "Data hash from service (" + serviceEndpoint + "): " + fileChecksumFromService);

        return checksum.equals(fileChecksumFromService);
    }

    private String loadChecksumFromService(String endpoint) {
        String fileChecksumFromService = null;
        try {
            fileChecksumFromService = new CypherAsyncTask(endpoint).execute().get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        return fileChecksumFromService;
    }
}
