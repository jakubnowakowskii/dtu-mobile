package pl.wat.wcy.checksumprojectmain;

import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class BanActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ban);
        Toast.makeText(this, R.string.integration_test_failure, Toast.LENGTH_LONG).show();
        finishAffinity();
    }
}
