package pl.wat.wcy.checksumprojectmain;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import pl.wat.wcy.checksumprojectmain.domain.AppDatabase;
import pl.wat.wcy.checksumprojectmain.utils.DataIntegrityTester;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppDatabase.getDatabase(this);
        boolean integrityTestPassed = new DataIntegrityTester().testForIntegrity(getApplicationContext());
        handleIntegrityTestResult(integrityTestPassed);
    }

    private void handleIntegrityTestResult(boolean integrityTestPassed) {
        if (integrityTestPassed) {
            startNewActivity(FineActivity.class);
        } else {
            startNewActivity(BanActivity.class);
        }
    }

    private <T extends Activity> void startNewActivity(Class<T> activityClass) {
        Intent intent = new Intent(getApplicationContext(), activityClass);
        startActivity(intent);
    }
}