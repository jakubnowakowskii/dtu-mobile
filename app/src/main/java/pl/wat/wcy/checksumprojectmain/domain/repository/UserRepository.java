package pl.wat.wcy.checksumprojectmain.domain.repository;

import android.content.Context;
import android.os.AsyncTask;

import java.lang.ref.WeakReference;
import java.util.Collections;
import java.util.List;

import pl.wat.wcy.checksumprojectmain.domain.entity.User;

import static java.util.Objects.nonNull;
import static pl.wat.wcy.checksumprojectmain.domain.AppDatabase.getDatabase;

public class UserRepository {

    public AsyncTask<Void, Void, List<User>> getAll(Context context) {
        return new GetAllUsersAsyncTask(context).execute();
    }

    private static class GetAllUsersAsyncTask extends AsyncTask<Void, Void, List<User>> {

        private WeakReference<Context> context;

        GetAllUsersAsyncTask(Context context) {
            this.context = new WeakReference<>(context);
        }

        @Override
        protected List<User> doInBackground(Void... voids) {
            if (nonNull(context.get())) {
                return getDatabase(context.get()).userDao().getAll();
            }
            return Collections.emptyList();
        }
    }
}
