# Data Integrity Tester

### Cel

Projekt ma na celu sprawdzanie integralności zasobów aplikacji mobilnej.

### Uruchomienie

Projekt należy zaimportowac w Android Studio, zbudować aplikację i uruchomić na dowolnym urządzeniu (bądź emulatorze) z systemem w wersji >= 24 (zalecane wersje >= 28) - proces budowania oraz uruchamiania aplikacji przedstawiony został na prezentacji wideo.

### Logowanie

Logów dotyczących sprawdzania skrótów zasobów należy szukać pod tagiem `DataIntegrityTester`.

### Prezentacja wideo

Prezentację z działania aplikacji można znaleźć pod [tym linkiem](https://youtu.be/VMu4fI-V_DA).
